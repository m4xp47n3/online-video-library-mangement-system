package services;


import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class DBObjectConnectionPool {

    private static HashMap<Connection, Boolean> DBpool =new HashMap<Connection, Boolean>();
    private Set<Connection> set=null;
    private Iterator<Connection> iter;
    private static final int poolSize=10;   
    Connection[] dbConn;
    boolean gotConnection = false;

    public void initializeConnectionPool(){
        if(DBpool!=null && DBpool.isEmpty()){
            try {
                dbConn=new Connection[poolSize]; 
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                String connQuery = "jdbc:mysql://localhost/video_library_system";  

                //Only one thread should initialize connections
                synchronized(this) {
                    for(int i=0;i<poolSize;i++) {
                        dbConn[i] = DriverManager.getConnection(connQuery,"root","root");
                        DBpool.put(dbConn[i], false);
                    }
                }
                System.out.println("DB connection successful: Total connections available: "+dbConn.length);
                
            }
            catch(Exception e){
                e.printStackTrace();
            }
            set=DBpool.keySet();
        }
        else
            System.out.println("Object Pool Map is already loaded with connections");
    }
    public Object getConnectionFromPool(){
        Connection con=null;
        if(set!=null && !set.isEmpty()) {
            iter=set.iterator();
            while(iter.hasNext()) {
                con=(Connection)(iter.next());
                if(DBpool.get(con)==false)  { 
                    System.out.println("This connection is unused, returning to client: "+con);
                    DBpool.put(con,true);  
                    return con;
                }
            }
            
            // Need to add code here to create a new connection incase all the connections in the pool are occupied
            
        }
        return con;
    }
    public void returnConnectionToPool(Connection con){
            if(DBpool.get(con)==true)  { //if true, means connection is is use till now
            System.out.println("Marking this connection as ready to use again: "+con);
            DBpool.put(con, false);  //Now making this connection as used.
        }
        else
            System.out.println("Nothing is done for this connection");

    }
}
